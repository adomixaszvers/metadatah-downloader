@echo off

title Metadatah Downloader

set BIN=%CD%\venv\Scripts

call %BIN%\activate.bat
python main.py %1 %2 %3
call %BIN%\deactivate.bat
