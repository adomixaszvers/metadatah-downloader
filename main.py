#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from os import path, makedirs

import requests
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache

SAVE_DIR = 'w:\\Exchange\\0_metainformacijos aprašai\\_Metainformacijos_aprasu_saugykla\\'

def create_session():
    sess = requests.session()
    forever_cache = FileCache('.web_cache', forever=True)
    return CacheControl(sess, forever_cache)


def get_doc_list(session, prefix_path):
    return session.get(prefix_path + '/published').json()


def get_and_save_doc(session, prefix_path, docId, filename):
    url = prefix_path + '/showStatic/%d/7' % docId
    response = session.get(url)
    if not response.from_cache and response.ok:
        docText = response.text
        with open(path.join(SAVE_DIR, filename), 'w', encoding='utf-8') as file:
            file.write(docText)
    print(response.status_code, filename, '(Iš podėlio)' if response.from_cache else '(Atsiųstas)')


def touchDir(dirName):
    if not path.exists(dirName):
        makedirs(dirName)


def main():
    if len(sys.argv) != 2:
        print("""Naudojimas:
main.py [kontekstas]
pvz. main.py http://localhost:8080/metadatah""")
        return
    prefix_path = sys.argv[1]
    session = create_session()
    try:
        doc_list = get_doc_list(session, prefix_path)
        for doc in doc_list:
            title = doc['title']
            docId = doc['id']
            get_and_save_doc(session, prefix_path, docId, title + '.html')
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print('Šakės')
        print(e)
        sys.exit(1)


if __name__ == "__main__":
    main()
