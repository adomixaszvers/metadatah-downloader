# README #

Reikia įdiegti 3 kartos Python. Atsisiųsti galima iš čia https://www.python.org/downloads/ . Diegiant svarbu pasirinkti, kad diegtų ir PIP.

Vėliau per PIP įdiegiamas VirtualEnv:
```
pip install virtualenv
```

Projekto aplanke:
```
virtualenv venv
venv\Scripts\activate
pip install -r requirements.txt
venv\Scripts\deactivate
```

Galiausiai paleidžiamas run.bat, pavyzdžui:
```
run.bat user password http://localhost:8080/metadatah
```